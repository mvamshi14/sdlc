package org.vamshi.jmsProducer.model;

import org.springframework.stereotype.Component;

@Component
public class Vendor {

	private String vendorName;
	private String firstName;
	private String lastName;
	private String city;
	private String zipCode;
	private String state;
	private String address;
	private String phoneNumber;
	private String email;
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	@Override
	public String toString() {
		return "Vendor [vendorName=" + getVendorName() + ", firstName=" + getFirstName() + ", lastName=" + getLastName() + ", city="
				+ getCity() + ", zipCode=" + getZipCode() + ", state=" + getState() + ", address=" + getAddress() + ", phoneNumber="
				+ getPhoneNumber() + ", email=" + getEmail() + "]";
	}
	
	
}
