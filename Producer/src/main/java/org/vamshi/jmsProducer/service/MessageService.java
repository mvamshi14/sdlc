package org.vamshi.jmsProducer.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.vamshi.jmsProducer.model.Vendor;
import org.vamshi.jmsProducer.sender.MessageSender;

import com.google.gson.Gson;

@Component
public class MessageService {

	@Autowired
	private MessageSender messageSender;
	private static Logger logger = LogManager.getLogger(MessageService.class.getName());
	public void process(Vendor vendor) {
		Gson gson = new Gson();
		String json = gson.toJson(vendor);
		messageSender.send(json);
		
	}

}
