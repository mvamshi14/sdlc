package org.vamshi.jmsProducer.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.vamshi.jmsProducer.model.Vendor;
import org.vamshi.jmsProducer.service.MessageService;

@Controller
public class ProducerController {

	private static Logger logger = LogManager.getLogger(ProducerController.class.getName());
	
	@Autowired
	private MessageService messageService;

	@RequestMapping("/")
	public String rendorVendorPage(Vendor vendor, Model model) {
		logger.info("Rendering index jsp");
		return "index";
	}

	@RequestMapping(value="/vendor", method=RequestMethod.POST)
	public ModelAndView processRequest(@ModelAttribute("vendor") Vendor vendor, Model model) {
		logger.info("Processing vendor request");
		messageService.process(vendor);
		logger.info(vendor.toString());
		ModelAndView mv = new ModelAndView();
		mv.setViewName("index");
		mv.addObject("message", "Vendor added Successfully");
		vendor = new Vendor();
		mv.addObject("vendor",vendor);
		return mv;
	}
}
