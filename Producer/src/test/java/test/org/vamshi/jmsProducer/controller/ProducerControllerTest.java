package test.org.vamshi.jmsProducer.controller;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;
import org.vamshi.jmsProducer.controller.ProducerController;
import org.vamshi.jmsProducer.model.Vendor;

public class ProducerControllerTest {

	
	private Vendor vendor;
	private Model model;
	private ProducerController producerController;
	private ApplicationContext appContext;
	@Before
	public void setUp() throws Exception {
	 appContext = new ClassPathXmlApplicationContext("spring/application-config.xml");
		 producerController = appContext.getBean("producerController", ProducerController.class);
		vendor = new Vendor();
		vendor.setAddress("Californai");
		vendor.setCity("LA");
		vendor.setEmail("abc@gmail.com");
		vendor.setFirstName("Bob");
		vendor.setLastName("Dob");
		vendor.setPhoneNumber("123467984");
		vendor.setState("California");
		vendor.setVendorName("UPS");
		vendor.setZipCode("13245");
		
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testRendorVendorPage() {
		
		assertEquals("index",producerController.rendorVendorPage(vendor, model));
	}

	@Test
	public void testProcessRequest() {
		ModelAndView mv = producerController.processRequest(vendor, model);
		assertEquals("index",mv.getViewName());
		
	}

}
