package test.org.vamshi.listener;

import static org.junit.Assert.*;

import javax.jms.JMSException;
import javax.jms.TextMessage;

import org.apache.xbean.spring.context.ClassPathXmlApplicationContext;
import org.easymock.EasyMock;

import static org.easymock.EasyMock.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.vamshi.adapter.ConsumerAdapter;
import org.vamshi.listener.ConsumerListener;

public class ConsumerListenerTest {

	
	private TextMessage message;
	private ApplicationContext context;
	private ConsumerListener listener;
	
	
	private String json = "{vendorName:\"Microsofttest3\",firstName:\"BobTest3\",lastName:\"SmithTest\",address:\"123 Main test\",city:\"TulsaTest\",state:\"OKTest\",zip:\"71345Test\",email:\"Bob@microsoft.test\",phoneNumber:\"test-123-test\"}";

	
	@Before
	public void setUp() throws Exception {
		context = new ClassPathXmlApplicationContext("/spring/application-config.xml");
		 listener=context.getBean("ConsumerListener",ConsumerListener.class);
	message = createMock(TextMessage.class);
	}

	@After
	public void tearDown() throws Exception {
		
		((ConfigurableApplicationContext) context).close();
	}

	@Test
	public void testOnMessage() throws JMSException {
       expect(message.getText()).andReturn(json);
       // replay will make the mock object available
       replay(message);
		listener.onMessage(message);
		//verfiy will check if mock object was called as defined
		verify(message);
	}

}
