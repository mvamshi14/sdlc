package org.vamshi.listener;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;
import org.vamshi.adapter.ConsumerAdapter;



@Component
public class ConsumerListener implements MessageListener {

	private static Logger  logger = LogManager.getLogger(ConsumerListener.class.getName());
	@Autowired
	JmsTemplate jmsTemplate;
	
	@Autowired 
	ConsumerAdapter consumerAdapter;
	
	@Override
	public void onMessage(Message message) {
		String json = null;
		
		logger.info("InOnMessage()");
		
		if (message instanceof TextMessage){
			try {
				json =  ((TextMessage) message).getText();
				logger.info("sending json to DB");
				consumerAdapter.sendToMongo(json);
			} catch (JMSException e) {
				logger.error("Message:" +json);
				jmsTemplate.convertAndSend(json);
				
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		
	}

}
