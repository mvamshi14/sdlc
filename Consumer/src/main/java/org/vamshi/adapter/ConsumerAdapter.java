package org.vamshi.adapter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.springframework.stereotype.Component;
import org.vamshi.listener.ConsumerListener;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.util.JSON;

@Component
public class ConsumerAdapter {

	/*public ConsumerAdapter() {
		Map<String, String> map = new HashMap<String, String>();
		String jsonData = null;
		String json = "{vendorName:\"Microsofttest\",firstName:\"BobTest\",lastName:\"SmithTest\",address:\"123 Main test\",city:\"TulsaTest\",state:\"OKTest\",zip:\"71345Test\",email:\"Bob@microsoft.test\",phoneNumber:\"test-123-test\"}";

		map.put(jsonData, json);
	}*/
	String json = "{vendorName:\"Microsofttest\",firstName:\"BobTest\",lastName:\"SmithTest\",address:\"123 Main test\",city:\"TulsaTest\",state:\"OKTest\",zip:\"71345Test\",email:\"Bob@microsoft.test\",phoneNumber:\"test-123-test\"}";

	private static Logger logger = LogManager.getLogger(ConsumerAdapter.class
			.getName());

	public void sendToMongo(String json) {

		logger.info("sending to mongodb");
		MongoClient client = new MongoClient();
		
		MongoDatabase database = client.getDatabase("vendor");
		MongoCollection<Document> collection = database
				.getCollection("contact");

		logger.info("converting json to DB ojb");

		if (Document.parse(json) != null) {
			collection.insertOne(Document.parse(json));
		}
		logger.info("sent to mongodb");
		client.close();
	}

}
